const bcrypt = require('bcryptjs');
const mongoose = require('mongoose');

const AdminSchema = new mongoose.Schema(
  {
    accName: {
      type: String,
      unique: true,
      min: [4, 'Too short, min is 4 characters'],
      max: [32, 'Too long, max is 32 characters'],
      lowercase: true
    },

    password: {
      type: String,
      min: [6, 'Password is too short, min is 6 characters']
    },

    remember_token: {
        type: String,
    },
  },
  {
    timestamps: true
  }
);

AdminSchema.methods.hasSamePassword = function(requestedPassword) {
  return bcrypt.compareSync(requestedPassword, this.password);
};

AdminSchema.pre('save', async function(next) {
  const admin = this;
  if (admin.isModified('password')) {
    try {
      const salt = await bcrypt.genSalt(10);
      admin.password = await bcrypt.hash(admin.password, salt);
      next();
    } catch (err) {
      console.log(err);
      next();
    }
  }
});

module.exports = mongoose.model('Admin', AdminSchema);
