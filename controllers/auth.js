const authService = require('../services/auth');

exports.signup = async (req, res) => {
  const user = await authService.signup(req.body);

  return res.render('auth/login', { success: true, user });
};